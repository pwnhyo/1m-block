
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <errno.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include "parse_header.h"
#include <string.h>

char *site_list_filename;
char **site_list;
int site_count = 0;

/* returns packet id */
static uint32_t print_pkt (struct nfq_data *tb)
{
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	uint32_t mark, ifi, uid, gid;
	int ret;
	unsigned char *data, *secdata;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph) {
		id = ntohl(ph->packet_id);
		//printf("hw_protocol=0x%04x hook=%u id=%u ",
		//	ntohs(ph->hw_protocol), ph->hook, id);
	}

	hwph = nfq_get_packet_hw(tb);
	if (hwph) {
		int i, hlen = ntohs(hwph->hw_addrlen);

		//printf("hw_src_addr=");
		//for (i = 0; i < hlen-1; i++)
		//	printf("%02x:", hwph->hw_addr[i]);
		//printf("%02x ", hwph->hw_addr[hlen-1]);
	}

	mark = nfq_get_nfmark(tb);
	//if (mark)
	//	printf("mark=%u ", mark);

	ifi = nfq_get_indev(tb);
	//if (ifi)
	//	printf("indev=%u ", ifi);

	ifi = nfq_get_outdev(tb);
	//if (ifi)
	//	printf("outdev=%u ", ifi);
	ifi = nfq_get_physindev(tb);
	//if (ifi)
	//	printf("physindev=%u ", ifi);

	ifi = nfq_get_physoutdev(tb);
	//if (ifi)
	//	printf("physoutdev=%u ", ifi);

	//if (nfq_get_uid(tb, &uid))
	//	printf("uid=%u ", uid);

	//if (nfq_get_gid(tb, &gid))
	//	printf("gid=%u ", gid);

	ret = nfq_get_secctx(tb, &secdata);
	//if (ret > 0)
	//	printf("secctx=\"%.*s\" ", ret, secdata);

	ret = nfq_get_payload(tb, &data);
	
	//if (ret >= 0)
	//	printf("payload_len=%d ", ret);

	//fputc('\n', stdout);

	return id;
}
	
void DumpHex(const void *data, int size)
{ // for dump packet
  char ascii[17];
  int i, j;
  ascii[16] = '\0';
  for (i = 0; i < size; ++i)
  {
    printf("%02X ", ((unsigned char *)data)[i]);
    if (((unsigned char *)data)[i] >= ' ' && ((unsigned char *)data)[i] <= '~')
    {
      ascii[i % 16] = ((unsigned char *)data)[i];
    }
    else
    {
      ascii[i % 16] = '.';
    }
    if ((i + 1) % 8 == 0 || i + 1 == size)
    {
      printf(" ");
      if ((i + 1) % 16 == 0)
      {
        printf("|  %s \n", ascii);
      }
      else if (i + 1 == size)
      {
        ascii[(i + 1) % 16] = '\0';
        if ((i + 1) % 16 <= 8)
        {
          printf(" ");
        }
        for (j = (i + 1) % 16; j < 16; ++j)
        {
          printf("   ");
        }
        printf("|  %s \n", ascii);
      }
    }
  }
}

int compare(const void *a, const void *b) {
    return strcmp(*(const char **)a, *(const char **)b);
}

int binary_search(char *arr[], int size, const char *target) {
    int left = 0;
    int right = size - 1;
    int mid;

    while (left <= right) {
        mid = left + (right - left) / 2;
        int cmp = strcmp(arr[mid], target);

        if (cmp == 0) {
            return 1;
        } else if (cmp < 0) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    return 0;
}

int search_filter_site(const char *check_host) {
    return binary_search(site_list, site_count, check_host);
}

int my_filter(struct nfq_data *tb){
	int len;
	unsigned char *data;

	struct nfqnl_msg_packet_hdr *ph;
	ph = nfq_get_msg_packet_hdr(tb);

	len = nfq_get_payload(tb, &data); // get data

    if (ntohs(ph->hw_protocol) == 0x0800) 
    { // if IPv4
      // Parse IP
      struct libnet_ipv4_hdr *ipv4_hdr = (struct libnet_ipv4_hdr *)data;

	  
      if (ipv4_hdr->ip_p == 6)
      { // if TCP
        // Parse TCP
		data += ipv4_hdr->ip_hl * 4;
        struct libnet_tcp_hdr *tcp_hdr = (struct libnet_tcp_hdr *)data;

		int payload_len;
		payload_len = ntohs(ipv4_hdr->ip_len) - (ipv4_hdr->ip_hl * 4) - (tcp_hdr->th_off * 4);
        // Parse Payload

        unsigned char *payload = malloc(sizeof(payload_len));

        memcpy(payload, data + (tcp_hdr->th_off * 4), payload_len);

		//DumpHex(payload, payload_len);

		char *check_host = strstr(payload, "Host: ");
		free(payload);

		if (check_host != NULL){
			check_host += 6;
			char *newline_ptr = strchr(check_host, '\r');

			if (newline_ptr != NULL) {
				*newline_ptr = '\0';
			}

			int filtered = search_filter_site(check_host);
			
			if (filtered){
				printf("[filtered] : %s\n", check_host);
				return 0;
			}
			else{
				return 1;
			}
		}
		
        
      }
    }

	
	return 1;
}

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	uint32_t id = print_pkt(nfa);
	//printf("entering callback\n");
	/////////////////////////////////////////
	// check http routine
	int forward = my_filter(nfa);

	if (forward){
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
	}
	else {
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	}
	/////////////////////////////////////////
}

void parse_and_sort_sites() {
    FILE *file = fopen(site_list_filename, "r");
    if (file == NULL) {
        fprintf(stderr, "Failed to open file: %s\n", site_list_filename);
        exit(1);
    }

    int max_sites = 1000000;
    site_list = malloc(max_sites * sizeof(char *));
	
    char line[1024];
    char *domain;

    while (fgets(line, sizeof(line), file) != NULL) {
        domain = strchr(line, ',');
        if (domain != NULL) {
            domain++; 
            domain[strcspn(domain, "\n")] = '\0'; 

            site_list[site_count] = strdup(domain);
            site_count++;
        }
    }
    fclose(file);

    qsort(site_list, site_count, sizeof(char *), compare);
}

int main(int argc, char **argv)
{
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	int fd;
	int rv;
	uint32_t queue = 0;

	char buf[4096] __attribute__ ((aligned));
	
	if (argc == 2) {
		site_list_filename = argv[1];
	}
	else {
		printf("Usage: 1m-block <site list file>");
		return -1;
	}

	parse_and_sort_sites();

	printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	printf("binding this socket to queue '%d'\n", queue);
	qh = nfq_create_queue(h, queue, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	printf("setting flags to request UID and GID\n");
	if (nfq_set_queue_flags(qh, NFQA_CFG_F_UID_GID, NFQA_CFG_F_UID_GID)) {
		fprintf(stderr, "This kernel version does not allow to "
				"retrieve process UID/GID.\n");
	}

	printf("setting flags to request security context\n");
	if (nfq_set_queue_flags(qh, NFQA_CFG_F_SECCTX, NFQA_CFG_F_SECCTX)) {
		fprintf(stderr, "This kernel version does not allow to "
				"retrieve security context.\n");
	}

	printf("Waiting for packets...\n");

	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			//printf("pkt received\n");
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. Please, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);


	exit(0);
}
